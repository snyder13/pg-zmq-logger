CXX ?= g++
CXXFLAGS = -Wall -Wextra -Weffc++ -std=c++11 -O2 -pedantic
LIBS = -lzmq -lpqxx -lboost_system -lboost_chrono -lboost_program_options -lsodium

all: pg-zmq-logger broker keygen

pg-zmq-logger: pg-zmq-logger.cc vendor/wrapidjson/wrapidjson.h
	${CXX} ${CXXFLAGS} pg-zmq-logger.cc -o pg-zmq-logger ${LIBS}

broker: broker.cc
	${CXX} ${CXXFLAGS} broker.cc -o broker ${LIBS}

keygen: keygen.cc
	${CXX} ${CXXFLAGS} keygen.cc -o keygen ${LIBS}

vendor/wrapidjson/wrapidjson.h:
	-mkdir vendor
	-cd vendor && git clone http://bitbucket.org/snyder13/wrapidjson.git

clean:
	rm pg-zmq-logger
