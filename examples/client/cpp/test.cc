#include <iostream>

#include "log.h"

int main(int, char**) {
	PgZmq::LogOpts opts("c++ test");
	auto log = opts
//		.logPath("tcp://localhost:5555") // default
//		.syncPath("tcp://localhost:5556") // default, if logPath is specified and has a numeric report it is derived to be one port higher
		.logger();

	for (size_t idx = 0; idx < 1; ++idx) {
		log.emergency()
			.set("string", "asdf")
			();
	}

	log.close();
}
