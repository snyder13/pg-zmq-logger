"use strict";
const
	pg = require('pg'),
	log = require('../../../client-libs/js/log.js')({
		'app': 'js example'
	}),
	randIp = () => {
		return [0,0,0,0].map(() => { return Math.floor(Math.random()*256); }).join('.');
	}
	;

log.access({ 'msg': 'test', 'network': { 'client_ip': randIp() } });
// logs something like
// {"app":"js example","msg":"test","network":{"client_ip":"142.169.166.59"},"timestamp":"2016-01-29EST12:04:00.437444-0500"}

log.close();
