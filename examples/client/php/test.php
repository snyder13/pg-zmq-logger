<?php

function randomIP() {
	return implode('.', array_map(function() { return rand(1, 256); }, [0,0,0,0]));
}

require __DIR__.'/../../../client-libs/php/log.php';

$log = \PgZmq\log()
	->app('php example') // arbitrary name for the application to ID message sources
//	->server("tcp://localhost:5555") // this is the default
//	->threads(1) // rule of thumb is 1 IO thread per gb per second, which is the default, so probably don't bother specifying this
	->call(); // done supplying options, get instance

// call the function named for the relevant log level
// access, debug, emergency, error, info, warning
$log->access([
	'msg' => 'test',
	'network' => [
		'client_ip' => randomIP()
	]
]);
// logs something like
// {"app":"php example","msg":"test","network":{"client_ip":"142.169.166.59"},"timestamp":"2016-01-29EST12:04:00.437444-0500"}
// in the file/table/bin for access-level messages
