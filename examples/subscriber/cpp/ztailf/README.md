# ztailf

Watch the event stream of the zeromq log channels that are of interest to you. Behaves like `tail -f`

## Usage

`ztailf channel [channel2, channel3, ...] [event address, default: tcp://localhost:5556]`

Channels are of the form "application name", "log level", or "application name:log level".

For example:

`ztailf ident:error emergency` -- listen to error-level events emitted by the ident app, and emergency events of any source
