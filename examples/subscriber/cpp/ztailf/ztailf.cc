#include <iostream>
#include <map>
#include <string>

#include <boost/regex.hpp>
#include <zmq.hpp>

int main(int argc, char** argv) {
	if (argc < 2) {
		std::cerr << "please supply channel(s) to subscribe to, and, optionally, the address to connect to (default tcp://localhost:5556)" << "\n";
		return 1;
	}
	zmq::context_t context(1);

	zmq::socket_t subscriber(context, ZMQ_SUB);
	std::string sockAddr = "tcp://localhost:5556";
	static const boost::regex urlish(".*://.*");
	for (auto idx = 1; idx < argc; ++idx) {
		std::string channel(argv[idx]);
		if (boost::regex_match(channel, urlish)) {
			sockAddr = channel;
			break;
		}
		subscriber.setsockopt(ZMQ_SUBSCRIBE, channel.c_str(), channel.size());
	}
	subscriber.connect(sockAddr.c_str());

	std::map<std::string, bool> seen;
	for (;;) {
		// unused but still gotta read it
		zmq::message_t _channel;
		subscriber.recv(&_channel);

		zmq::message_t id;
		subscriber.recv(&id);
		// read whole message,
		zmq::message_t req;
		subscriber.recv(&req);
		// then check for dupes and skip as indicated
		std::string sid(static_cast<char*>(id.data()), id.size());
		if (seen.find(sid) != seen.end()) {
			continue;
		}
		seen[sid] = true;

		std::string logDoc(static_cast<char*>(req.data()), req.size());
		std::cout << logDoc << std::endl;
	}
}
