var MAX_LEN = 400,
	mail = require('nodemailer'),
	zmq = require('zmq'),
	sock = zmq.socket('sub'),
	conf = JSON.parse(require('fs').readFileSync('secrets.json'))
	;

if (!process.argv[2]) {
	console.error('please supply a channel name in the form app, level, or app:level, and optionally the socket address to connect to (default: tcp://localhost:5556)');
	process.exit();
}

var mailTransport = mail.createTransport('smtps://' + encodeURIComponent(conf.from) + ':' + encodeURIComponent(conf.pass) + '@' + conf.smtp);

sock.on('message', function(channel, doc) {
	mailTransport.sendMail({
		'from': conf.from,
		'to'  : conf.to,
		'text': doc.toString('utf8').substr(0, MAX_LEN)
	}, function(err, info) {
		if (err) {
			console.log(err);
			return;
		}
	});
});

sock.connect(process.argv[3] ? process.argv[3] : "tcp://localhost:5556");
sock.subscribe(process.argv[2]);

process.on('SIGINT', function() {
	sock.close()
});
