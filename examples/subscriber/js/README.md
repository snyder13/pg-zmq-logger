# JS Subscriber Example -- Send Matching Messages via SMS
## Installation
* `npm install` for dependencies

## Configuration
* Create `examples/subscribes/js/secrets.json`

    {
         "from": "your@address.com",
         "to": "NNNNNNNNNN@sms.gateway.com", // see https://en.wikipedia.org/wiki/SMS_gateway#Use_with_email_clients to find an email gateway for your provider
         "smtp": "smtp.address.com",
         "pass": "your password at smtp.address.com"
    }

## Running
* `node text.js $LOGLEVEL`
* OR `node text.js $APPNAME`
* OR `node text.js $APPNAME $LOGLEVEL`


