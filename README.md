# pg-zmq-logger

A  logger based on cramming arbitrary JSON documents into PostgreSQL and/or a "regular" log file using ZeroMQ to collect entries.

A C++ server listens on (default) port 5555, and 5556.

Logging applications send JSON documents to :5555, which are expected to define string keys for "app", a unique id for the source of the messages, and "level", the log level of the message.

The messages are logged according to a the strategy defined at launch, either to PostgreSQL or to a flat file.

Additionally, events are posted the :5556 socket according to their app name and log level, so that other processes might make use of those messages.

To get an idea of how this works at a higher level, there are example clients in a few languages in the `examples/client` path, and an example of an application that does additional routing of log messages in `examples/subscriber`.

## Files
* `pg-zmq-logger.cc` simple server
* `client-libs/` decent API to publish to the server in a few different languages
* `examples/client` "decent API" usage
* `examples/subscriber` the core logger can multiplex messages to any number of subscribers based on application name, log level, or both. These are examples of how to implement something in that vein. See the README.md files there for particular requirements.

## Building

* Install dependencies using your package manager: libzmq-dev libpqxx-dev libboost-chrono-dev libboost-system-dev libboost-program-options-dev
* Run `make`, or if you prefer `CXX=clang++ make`

## Configuration
### Log to file
* Create a file that is writable to the user running the server
       * `touch /var/log/zmq/fallback.log` (default)
* This is required even when using PostgreSQL as a failsafe

### Log to PostgreSQL (optional)
* Make a log file writable to the user running the server, by default `/var/log/pg-zmq-fallback.log`. This is used if the database connection is not working for some reason.
* Create a postgres user with a password, or choose an existing one: `CREATE USER logs WITH PASSWORD 'logs'`
* Create a database or choose an existing one: `CREATE DATABASE logs`
* Create a table in any pg database: `CREATE TABLE access(id serial not null primary key, doc jsonb not null)`
* Grant permissions: `GRANT CONNECT ON DATABASE logs TO logs`, `GRANT ALL ON access TO logs`
* (possibly) Allow password authentication in pg_hba.conf. (The node database connector used in the example is flakey with ident auth. It will work with the C++ example)

## Running the server
* `pg-zmq-logger --help`

        Allowed options:
          --help                                produce help message
          --db arg                              postgres connection string
          --file arg (=/var/log/zmq/fallback.log)
                                                file to log to, either as primary or as
                                                a fallback for database logging
          --logSocket arg (=tcp://*:5555)       socket to accept log messages on
          --pubSocket arg (=tcp://*:5556)       socket to publish messages to (for
                                            plugins)


* The database connection string is optional, for the format see: http://www.postgresql.org/docs/9.5/static/libpq-connect.html#LIBPQ-CONNSTRING
* The file will either be used as the primary logging method, or used in the event of a connectivity issue with the database as a fallback
* logSocket is where clients connect to send messages
* pubSocket is where clients connect to do additional routing or processing of messages

## Running examples

### node.js - examples/js
* `npm install` in examples/js, and also `npm install` in client-libs/js
* `node ./test.js 1000` (the number is the number of test messages to send)

### C++ - examples/cpp
* Install additional dependencies: libboost-serialization-dev libboost-regex-dev rapidjson-dev
* `make` or `CXX=clang++ make`
* `./test`

### PHP - examples/php
* Install the zmq extension according to one of the methods here: http://zeromq.org/bindings:php
* `php ./test.php`

## Running subscribers
* See `examples/subscribers` for specific examples
* To make a new client, link the 0mq library, create a subscriber socket, connect to the log server's publish socket, and listen to the channels that interest you, in the form:
	* `app name`: messages from a given app.
	* `log level`: messages of a given log level.
	* `app name:log level`: messages of the given log level emitted by the given app

Note you can subscribe to multipe channels, and you can do further filtering by examining the JSON document sent in a given request.

### Subscriber example
This example sends messages matching the given application ID and/or log level as text message. You'll need to configure email-&gt;SMS routing. You can use a gmail address.

* Create `examples/subscribers/js/secrets.json`


        {
             "from": "your@address.com",
             "to": "NNNNNNNNNN@sms.gateway.com", # see https://en.wikipedia.org/wiki/SMS_gateway#Use_with_email_clients to find an email gateway for your provider
             "smtp": "smtp.address.com",
             "pass": "your password at smtp.address.com"
        }

* Run with `node text.js $LOGLEVEL`, `node text.js $APPNAME`, or `node text.js $APPNAME $LOGLEVEL`

