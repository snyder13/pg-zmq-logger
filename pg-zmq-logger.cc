#include <fstream>
#include <iostream>
#include <map>
#include <random>
#include <stdexcept>
#include <string>

#include <boost/chrono.hpp>
#include <boost/program_options.hpp>
#include <pqxx/connection.hxx>
#include <pqxx/prepared_statement.hxx>
#include <pqxx/transaction.hxx>
#include <pqxx/nontransaction.hxx>
#include <zmq.hpp>
#include <zmq_utils.h>

#include "vendor/wrapidjson/wrapidjson.h"
#include "sodium-helpers.h"

/**
 * base class for things that persist log messages
 *
 * must implement (app, logLevel, document) to put a message matching those
 * 	parameters into an appropriate storage.
 * 	returns a boolean indicating success
 *
 * should implement getLastError() to describe anything that went wrong with
 * 	the prior attempt to log a message.
 * 	return string is undefined when last log attempt was successful
 */
class LogStrategy
{
public:
	virtual ~LogStrategy() {}
	virtual auto operator()(const std::string& level, const std::string& doc) -> bool = 0;
	virtual auto getLastError() -> std::string = 0;
};

/** Log message to PostgreSQL tables named for the log level */
class PgLogStrategy : public LogStrategy {
	// multiplier for exponential backoff retrying lost db connection
	static const size_t BACKOFF_BASE_MS = 100;
	// max time to wait between reconnection attempts
	static const size_t BACKOFF_MAX_MS = 30000;
public:
	PgLogStrategy(const std::string& connStr) :
		dbh(new pqxx::connection(connStr)), work(*dbh), connStr(connStr), error(),
		connected(true), lastRetry(boost::chrono::high_resolution_clock::now()), retryCount(0), delay(0),
		rd(), gen(rd()) {
		prepareSQL();
	}
	PgLogStrategy(const PgLogStrategy&) = delete;
	auto operator=(const PgLogStrategy&) -> PgLogStrategy& = delete;
	~PgLogStrategy() {
		if (dbh) {
			delete dbh;
		}
	}

	auto operator()(const std::string& level, const std::string& doc) -> bool {
		try {
			if (connected) {
				work.prepared(level)(doc).exec();
				return true;
			}
			else {
				reconnect();
				return false;
			}
		}
		catch (const pqxx::pqxx_exception& ex) {
			connected = false;
			error = ex.base().what();
			return false;
		}
	}
	auto getLastError() -> std::string { return error; }
private:
	auto reconnect() -> void {
		// check whether the backoff wait period is expired
		boost::chrono::high_resolution_clock::time_point now = boost::chrono::high_resolution_clock::now();
		boost::chrono::milliseconds ms = boost::chrono::duration_cast<boost::chrono::milliseconds>(now - lastRetry);
		if (retryCount == 0 || (ms.count() > 0 && static_cast<size_t>(ms.count()) > delay)) {
			try {
				if (dbh) {
					delete dbh;
				}
				lastRetry = now;
				dbh = new pqxx::connection(connStr);
				// reset state
				prepareSQL();
				connected = true;
				retryCount = 0;
			}
			catch (const pqxx::broken_connection& ex) {
				// wait (on average) longer before trying again
				delay = BACKOFF_BASE_MS * std::uniform_int_distribution<>(0, retryCount)(gen);
				// but not too long
				if (delay > BACKOFF_MAX_MS) {
					delay = BACKOFF_MAX_MS;
				}
				++retryCount;
			}
		}
	}
	auto prepareSQL() -> void {
		// prepare some statement handles.
		// I haven't given any thought to the db schema, it's just a table per
		// log-level to test
		for (const auto& level: { "emergency", "error", "warning", "debug", "info", "access" }) {
			dbh->prepare(level, std::string("INSERT INTO ") + level + "(doc) VALUES ($1::jsonb)");
		}
	}
	pqxx::connection* dbh;
	pqxx::nontransaction work;
	const std::string connStr;
	std::string error;
	bool connected;
	boost::chrono::high_resolution_clock::time_point lastRetry;
	size_t retryCount;
	size_t delay;
	std::random_device rd;
	std::mt19937 gen;
};

/**
 * Log messages to flat files
 */
class FileLogStrategy : public LogStrategy {
public:
	static const std::string DEFAULT;
	FileLogStrategy(const std::string& fileName = DEFAULT) : fs(fileName.c_str(), std::ios_base::out|std::ios_base::app), error("") {
		if (!fs.good()) {
			throw std::runtime_error(std::string("unable to open log file ") + fileName);
		}
		fs.exceptions(fs.exceptions() | std::ios::failbit);
	}
	auto operator()(const std::string& level, const std::string& doc) -> bool {
		if (!fs.good()) {
			return false;
		}
		try {
			fs << level << ":" << doc << std::endl;
		}
		catch (std::ios_base::failure& ex) {
			error = ex.what();
			return false;
		}
		return true;
	}
	auto getLastError() -> std::string { return error; }
private:
	std::ofstream fs;
	std::string error;
};
const std::string FileLogStrategy::DEFAULT  = "/var/log/zmq/fallback.log";

/**
 * Interprocess communication
 */
class PgZmqLogger
{
private:
	// doc recommends one thread per gb per second
	static const size_t IO_THREADS = 1;

public:
	PgZmqLogger() : primary(nullptr), fallback(nullptr), shared(true, false), certs(false) {
	}

	// prevent unintentional copying
	auto operator=(const PgZmqLogger&) -> PgZmqLogger& = delete;
	PgZmqLogger(const PgZmqLogger&) = delete;

	auto setLogStrategy(LogStrategy* ls) -> PgZmqLogger& {
		primary = ls;
		return *this;
	}

	auto setFallbackLogStrategy(LogStrategy* ls) -> PgZmqLogger& {
		fallback = ls;
		return *this;
	}

	auto loadKeys(const std::string& serverPath, const std::string& clientPath) -> PgZmqLogger& {
		// load client's public key
		auto clientPubKey = PublicKey::fromFile(clientPath + ".pub");

		// load my keys
		auto server = KeyPair::fromFiles(serverPath);

		// diffie-hellman up a shared key
		shared = Hash::serverDiffieHellman(server, clientPubKey);
		server.getPrivateKey().destroy();
		certs = true;

		return *this;
	}

	auto startMessageLoop(const std::string& logBind, const std::string& pubBind, const std::string& sessBind) -> void {
		assert(primary);
		assert(certs);

		// set up zeromq
		zmq::context_t ctx(IO_THREADS);
		// pull socket for log messages
		zmq::socket_t logSock(ctx, ZMQ_PULL);
		logSock.bind(logBind.c_str());
		// publish socket for plugins listening for a specific category of messages
		zmq::socket_t pubSock(ctx, ZMQ_PUB);
		pubSock.bind(pubBind.c_str());
		// session negotiation socket
		zmq::socket_t hsSock(ctx, ZMQ_REP);
		hsSock.bind(sessBind.c_str());

		zmq::pollitem_t items[] = {
			{ logSock, 0, ZMQ_POLLIN, 0 },
			{ hsSock, 0, ZMQ_POLLIN, 0 }
		};

		Nonce nonce;
		SessionToken sessionId;
		std::map<SessionToken, Hash> sessions;

		// reusable buffers
		unsigned char ciphertext[MAX_MSG_SIZE];
		unsigned char decrypted[MAX_MSG_SIZE];
		sodium_mlock(decrypted, sizeof(decrypted));

		// reconnection materials
		for (;;) {
			zmq::message_t req;
			zmq::poll(items, 2, -1);

			// log message
   		if (items[0].revents & ZMQ_POLLIN) {
				// three-part message: app name, log level, arbitray json document
				logSock.recv(&req);

				// verify message is not too large for the reusable buffer or too small to possibly be anything
				if (req.size() > MAX_MSG_SIZE) {
					std::cerr << "error: message too large\n";
					continue;
				}
				if (req.size() < Nonce::size() + SessionToken::size() + crypto_secretbox_MACBYTES + 1) {
					std::cerr << "error: incomplete message\n";
					continue;
				}

				// nonce is concatenated with the rest of the message, take that many bytes to get it
				nonce.reset(static_cast<unsigned char*>(req.data()));

				// next field is the session id
				sessionId.reset(static_cast<unsigned char*>(req.data()) + nonce.size());


				auto it = sessions.find(sessionId);
				if (it == sessions.end()) {
					std::cerr << "error: invalid session";
					sessionId.hexLog("proposed session");
					for (const auto& sess: sessions) {
						sess.first.hexLog("\tvalid session");
					}
					continue;
				}

				// read the rest of the message as the payload
				size_t ciphertextLen = req.size() - nonce.size() - sessionId.size();

				// decrypt
				if (crypto_secretbox_open_easy(decrypted, static_cast<const unsigned char*>(req.data()) + nonce.size() + sessionId.size(), ciphertextLen, nonce, it->second) != 0) {
					std::cerr << "error: forged message\n";
					it->first.hexLog("\tsession");
					continue;
				}
				decrypted[ciphertextLen - crypto_secretbox_MACBYTES] = '\0';
				std::string level = "";
				size_t idx = 0;
				bool found = false;
				for (; idx < ciphertextLen - crypto_secretbox_MACBYTES - 2; ++idx) {
					if (decrypted[idx] == ':') {
						found = true;
						++idx;
						break;
					}
					level += decrypted[idx];
				}
				if (!found) {
					std::cerr << "error: malformed message\n";
					it->first.hexLog("\tsession");
					std::cerr << "\t" << decrypted << "\n";
					continue;
				}
				std::string logDoc(reinterpret_cast<const char*>(decrypted + idx));
				// log messages
				if (!(*primary)(level, logDoc)) {
					if (fallback) {
						std::cerr << "warning: primary log strategy failed, falling back\n" << primary->getLastError() << "\n";
					}
					else {
						throw std::runtime_error("primary log strategy failed, no fallback specified: \n" + primary->getLastError());
					}
					if (!(*fallback)(level, logDoc)) {
						throw std::runtime_error("both primary and fallback log strategies failed: " + fallback->getLastError());
					}
				}

				std::cout << "pub\n";
				// publish for other interested parties
				zmq_send(pubSock, "a", 1, ZMQ_SNDMORE);
				nonce.reset();
				auto encPubMsg = it->second.encrypt(logDoc.c_str());
				zmq::message_t resp(Nonce::size() + sessionId.size() + encPubMsg.size());
				memcpy(resp.data(), nonce, nonce.size());
				memcpy(static_cast<unsigned char*>(resp.data()) + nonce.size(), sessionId, sessionId.size());
				memcpy(static_cast<unsigned char*>(resp.data()) + nonce.size() + sessionId.size(), encPubMsg, encPubMsg.size());
				pubSock.send(resp);
			}

			// session message
   		if (items[1].revents & ZMQ_POLLIN) {
				hsSock.recv(&req);
				if (req.size() != Nonce::size() + PublicKey::size() + crypto_secretbox_MACBYTES) {
					std::cerr << "error: bad public key size\n";
					continue;
				}
				if (req.size() > MAX_MSG_SIZE) {
					std::cerr << "error: handshake message too large\n";
					continue;
				}

				// nonce is concatenated with the rest of the message, take that many bytes to get it
				nonce.reset(static_cast<unsigned char*>(req.data()));

				// read payload from rest of message
				size_t ciphertextLen = req.size() - crypto_secretbox_NONCEBYTES;
				memcpy(ciphertext, req.data() + crypto_secretbox_NONCEBYTES, ciphertextLen);

				// decrypt
				if (crypto_secretbox_open_easy(decrypted, ciphertext, ciphertextLen, nonce, shared) != 0) {
					std::cerr << "error: forged handshake message\n";
					continue;
				}

				// generate a pair of ephemeral keys
				auto session = KeyPair::random();

				// generate shared secret for the session
				auto sessShared = Hash::serverDiffieHellman(session, PublicKey::fromBytes(decrypted));

				// done with session private key
				session.getPrivateKey().destroy();

				// generate a session token
				SessionToken tok;

				// encrypt public key, session token
				auto sessionReplyCipher = shared.encrypt(session.getPublicKey());

				// send the public key
				zmq::message_t hs(nonce.size() + tok.size() + sessionReplyCipher.size());
				memcpy(hs.data(), sessionReplyCipher.getNonce(), Nonce::size());
				memcpy(static_cast<char*>(hs.data()) + nonce.size(), tok, tok.size());
				memcpy(static_cast<char*>(hs.data()) + nonce.size() + tok.size(), sessionReplyCipher, sessionReplyCipher.size());
				hsSock.send(hs);

				// store session secret by session id
				sessions.insert(std::make_pair(tok, sessShared));
			}
		}
	}
private:
	LogStrategy* primary;
	LogStrategy* fallback;
	Hash shared;
	bool certs;
};

namespace po = boost::program_options;

/**
 * Set up sockets
 */
int main(int argc, char** argv) {
	po::options_description desc("Allowed options");
	desc.add_options()
   	("help",     "produce help message")
		("db",       po::value<std::string>(), "postgres connection string")
		("file",     po::value<std::string>()->default_value(FileLogStrategy::DEFAULT), "file to log to, either as primary or as a fallback for database logging")
		("logSock",  po::value<std::string>()->default_value("tcp://*:5555"), "socket to accept log messages on")
		("pubSock",  po::value<std::string>()->default_value("tcp://*:5556"), "socket to publish messages to (for plugins)")
		("sessSock", po::value<std::string>()->default_value("tcp://*:5557"), "socket to set up encryption sessions")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cout << desc;
		return 0;
	}

	PgLogStrategy* pg;
	FileLogStrategy fs(vm["file"].as<std::string>());

	PgZmqLogger logger;
	if (vm.count("db")) {
		pg = new PgLogStrategy(vm["db"].as<std::string>());
		logger
			.setLogStrategy(pg)
			.setFallbackLogStrategy(&fs);
	}
	else {
		logger.setLogStrategy(&fs);
	}

	std::cout << "binding at logging - " << vm["logSock"].as<std::string>() << ", publishing - " << vm["pubSock"].as<std::string>() << ", session negotiation - " << vm["sessSock"].as<std::string>() << "\n";
	logger
		.loadKeys("keys/server", "keys/client")
		.startMessageLoop(vm["logSock"].as<std::string>(), vm["pubSock"].as<std::string>(), vm["sessSock"].as<std::string>());
}
