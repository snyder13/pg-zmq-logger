<?php namespace PgZmq;

require 'FluentArgs.php';

function log($opts = NULL) {
	if (!is_array($opts)) {
		return \Hz\fluentize('PgZmq\log', [
			'app'     => ['is_string', 'required'],
			'server'  => ['is_url', 'default' => 'tcp://localhost:5555'],
			'threads' => ['is_int', 'default' => 1, 'min' => 1]
		]);
	}
	return new Logger($opts);
}

function now() {
	$mt = microtime(TRUE);
	$micro = sprintf("%06d",($mt - floor($mt)) * 1000000);
	return date('Y-m-d', $mt).'T'.date('H:i:s.', $mt).$micro.date('O', $mt);
}

class Logger
{
	private static $logLevels = ['emergency' => 1, 'error' => 1, 'warning' => 1, 'debug' => 1, 'info' => 1, 'access' => 1];
	private $zctx, $sock, $app;

	// use \PgZmq::log() helper function instead of calling directly pls
	public function __construct($opts) {
		$this->app = $opts['app'];
		$this->zctx = new \ZMQContext($opts['threads']);
		$this->sock = new \ZMQSocket($this->zctx, \ZMQ::SOCKET_PUSH);
		$this->sock->connect($opts['server']);
	}

	// respond to methods that match recognized log levels
	public function __call($mtd, $args) {
		if (!isset(self::$logLevels[$mtd])) {
			throw new \Exception('invalid log level '.$mtd);
		}
		if (!count($args)) {
			throw new \Exception('no log message supplied');
		}
		$msg = $args[0];
		if (is_object($msg)) {
			$msg = (array)$msg;
		}
		if (!is_array($msg)) {
			$msg = ['message' => $msg];
		}
		if (!isset($msg['app'])) {
			$msg['app'] = $this->app;
		}
		if (!isset($msg['timestamp'])) {
			$msg['timestamp'] = now();
		}
		$this->sock->sendmulti([$this->app, $mtd, \Hz\toJson($msg)]);
	}
}
