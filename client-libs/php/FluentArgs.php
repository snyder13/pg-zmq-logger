<?php namespace Hz;

/**
 * Partial argument-applicator/named-argument container with guards.
 *
 * This is meant to reduce boilerplate for interfaces like:
 *
 * $obj->startOperation()
 * 	->someParam(10)
 * 	->someOtherParam($foo)
 * 	->call();
 *
 * by making it so that these functions do not actually have to be defined, and
 * their definitions can include some simple validations that might have
 * otherwise occurred in those functions;
 *
 * E.g.,
 *
 *class Greeter
 * {
 *	public static function greet($args = NULL) {
 *		static $opts = [
 *			'name' => [ 'required', 'maxLen' => 10 ],
 *			'greeting' => [ 'default' => 'Hello' ]
 *		];
 *		if (!is_array($args)) {
 *			return fluentize([get_class(), 'greet'], $opts);
 *		}
 *		echo "${args['greeting']}, ${args['name']}!";
 *	}
 *}
 *
 * Greeter::greet()->name('Steve')->call(); // Hello, Steve!
 * Greeter::greet()->greeting('Yo')->name('Steve Buscemi')->call(); // argument error(s): argument name failed validation 'maxLen/10'
 * Greeter::greet()->greeting('Yo')->name('Steve')->call(); // Yo, Steve!
 * Greeter::greet(['name' => 'Steve Buscemi']); // Undefined index: greeting -- , Steve Buscemi! -- it's ok to call like this but you have to supply all arguments and mind that validations may be skipped
 * Greeter::greet()->call(['name' => 'Steve Buscemi']); // faild validation maxLen/10 -- array syntax also allowed in call(), in which case validations are checked
 * Greeter::greet()->greeting('Yo')->call(['name' => 'Steve']); // Yo, Steve! -- or mix-and-match calling conventions
 * $userGreet = Greeter::greet()->name('Steve'); // partially-apply a function and save it for later
 * $userGreet->greeting('Hi')->call(); // Hi, Steve!
 * $userGreet->greeting('Bye')->call(); // Bye, Steve!
 *
 * One can also convert interfaces expecting an array of options to fluent
 * interfaces by wrapping them, without changing them. See the tests for
 * examples of how that works.
 */
class FluentArgs
{
	private $opts,          ///< array of FluentOpt in "intrusive mode", here this class collects named arguments and (optionally) applies some assertions to them
		$fun,                ///< wrapped callback. any callable is ok, doesn't need to be a Closure
		$args = array(),     ///< argument storage for intrusive/named arg mode
		$errors = array(),   ///< assertion failures when options are supplied that might complain about the arguments supplied
		$positional = FALSE; ///< whether to collect arguments in order (TRUE) or by name (FALSE)

	/**
	 * See fluentize() for usage rather than callling this directly, probably.
	 * @param fun callable backing function
	 * @param opts array of FluentOpt or else NULL, indicating wrapper/positional mode
	 */
	public function __construct(callable $fun, $opts, $positional) {
		$this->opts = $opts;
		$this->fun = $fun;
		$this->positional = $positional;
	}

	public function reset() {
		$this->args = [];
		return $this;
	}

	/**
	 * $fluentArgsInst->someArgumentName($someArgumentValue) sets someArgumentName to $someArgumentValue,
	 * or complains if the FluentOpt in use do not ask for such an argument.
	 * always fails in positional mode.
	 * @param mtd string argument name
	 * @param args array of mixed technically, but only the first element is used
	 * @return FluentArgs
	 */
	public function __call($mtd, $args) {
		if (isset($this->opts[$mtd]) && ($errs = $this->opts[$mtd]->getErrors($args))) {
			$this->errors = array_merge($this->errors, array_map(function($err) use($mtd) { return "argument $mtd failed validation '$err'"; }, $errs));
		}
		if (!isset($this->opts[$mtd])) {
			if (count($args) == 1) {
				list($this->args[$mtd]) = $args;
			}
			else {
				if (!array_key_exists($mtd, $this->args)) {
					$this->args[$mtd] = [];
				}
				$this->args[$mtd] = (array)$this->args[$mtd];
				$this->args[$mtd][] = $args;
			}
		}
		else {
			if ($this->opts[$mtd]->isMultiple()) {
				if (!array_key_exists($mtd, $this->args)) {
					$this->args[$mtd] = [];
				}
				$this->args[$mtd] = (array)$this->args[$mtd];
				$this->args[$mtd][] = $args;
			}
			else {
				list($this->args[$mtd]) = $args;
			}
		}
		return $this;
	}

	/**
	 * Run the wrapped callback.
	 * Though not listed in the function definition to avoid having to distinguish between a default NULL and someone actually trying to supply NULL,
	 * this accepts variable arguments.
	 * In positional mode, if supplied, these arguments are supplied "normally" and will fill in unbound arguments to the wrapped function left-to-right order as they appear.
	 * In named-arg mode, if supplied, there should be one argument that is an associative array giving the names of each parameter as keys to their values
	 * @return mixed
	 */
	public function call() {
		// positional mode doesn't involve validations, just glom the args together and call the wrapped function
		$ar = func_get_args();
		// apply any arguments supplied here
		if (isset($ar[0])) {
			foreach ($ar[0] as $k=>$v) {
				call_user_func(array($this, $k), $v);
			}
		}
		// validations were run for supplied arguments at the time they were supplied, but we can still do a little checking of those that are missing, if any
		foreach ($this->opts as $name=>$opt) {
			if (!array_key_exists($name, $this->args)) {
				// missing required arguments are an error even if they have a default!
				if ($opt->isRequired()) {
					$this->errors[] = 'missing required argument '.$name;
				}
				// optional arguments can get filled in with whatever the default is, maybe still NULL, but we'll at least set the key
				else {
					$this->args[$name] = $opt->getDefault($opt->isMultiple() ? [] : NULL);
				}
			}
		}
		// throwing errors later allows supplying a comprehensive list of problems, which could avoid some back-and-forth debugging
		if ($this->errors) {
			throw new \Exception('argument error(s): '. implode('; ', $this->errors));
		}
		if ($this->positional) {
			$optPos = [];
			$idx = 0;
			foreach ($this->opts as $k=>$_) {
				$optPos[$k] = $idx++;
			}
			uksort($this->args, function($a, $b) use($optPos) {
				return $optPos[$a] - $optPos[$b];
			});
		}
		// supply the arguments back to the wrapped function, which is hopefully noting that it received an array and that it shouldn't call back here again
		return $this->positional ? call_user_func_array($this->fun, $this->args) : call_user_func($this->fun, $this->args);
	}

	/**
	 * Run the wrapped callback, alternative syntax. See documentatin for call()
	 * @return mixed
	 */
	public function __invoke() {
		return $this->call(func_get_args());
	}
}

/**
 * Set of options for a given argument
 */
class FluentOpt
{
	private $opts = array();

	/**
	 * See docs for fluentize() rather than calling this directly, probably
	 * @param opts array of options
	 */
	public function __construct($opts = []) {
		foreach (is_string($opts) ? preg_split('/\s*,\s*/', $opts) : (array)$opts as $name=>$opt) {
			if (is_int($name)) {
				$name = $opt;
				$opt = NULL;
			}
			if ($name) {
				$this->opts[$name] = $opt;
			}
		}
	}

	/**
	 * @return boolean whether failing to supply this argument is an error
	 */
	public function isRequired() {
		return array_key_exists('required', $this->opts);
	}

	public function isMultiple() {
		return array_key_exists('multiple', $this->opts);
	}

	/**
	 * @return mixed default value, NULL if never specified
	 */
	public function getDefault($def = NULL) {
		return array_key_exists('default', $this->opts) ? $this->opts['default'] : $def;
	}

	/**
	 * Get a list of comlaints about a given value according to the options supplied to us
	 * @param val mixed
	 */
	public function getErrors($val) {
		static $validators = NULL;
		if (is_null($validators)) {
			$validators = [
				'maxLen'   => function($len, $val) { return strlen($val) <= $len; },
				'minLen'   => function($len, $val) { return strlen($val) >= $len; },
				'in'       => function($ar,  $val) { return in_array($val, $ar, TRUE); },
				'matching' => function($re,  $val) { return preg_match($re, $val); }
			];
		}
		static $pass = ['required' => 1, 'default' => 1, 'multiple' => []];

		$errs = array();
		foreach ($this->opts as $name=>$arg) {
			// some options are supplied as plain values, eg 'is_string', others are 'name' => 'argument to validator' as described above, and some are 'name' => callback. so, normalize a bit
			if (is_int($name)) {
				$name = $arg;
			}
			if (!$arg) {
				$arg = $name;
			}
			// pass options that have irrelevant meanings here
			if (isset($pass[$name])) {
				continue;
			}
			// delegate to a callback
			if (is_callable($arg)) {
				if (!call_user_func($arg, $this->isMultiple() ? $val : $val[0])) {
					$errs[] = $name.($arg == $name ? '' : '/'.json_encode($arg, 1));
				}
			}
			// or, use a built-in
			else if (array_key_exists($name, $validators)) {
				if (!call_user_func_array($validators[$name], [$arg, $this->isMultiple() ? $val : $val[0]])) {
					$errs[] = $name.($arg == $name ? '' : '/'.json_encode($arg, 1));
				}
			}
			// or, fail if we don't know how to handle an option. this is an error on the level of the wrapped function and not the caller, so it's appropriate to bail now rather than pushing an error insinuating they did something wrong supplying args
			else {
				throw new \Exception('unknown validation '.$name);
			}
		}
		return $errs;
	}
}

function fluentize($fun, $opts = NULL) {
	$positional = [];
	// can create a default set of options that just requires the things that are required by the function definition
	if (is_null($opts)) {
		$refl = is_string($fun) || $fun instanceof \Closure ? new ReflectionFunction($fun) : new ReflectionMethod($fun[0], $fun[1]);
		$opts = [];
		foreach ($refl->getParameters() as $param) {
			if ($param->isOptional()) {
				$opts[$param->name] = ['default' => $param->getDefaultValue()];
			}
			else {
				$opts[$param->name] = ['required'];
			}
			$positional[] = $param->name;
		}
	}
	return new FluentArgs($fun, is_null($opts) ? NULL : array_map(function($opt) {
		return new FluentOpt($opt);
	}, $opts), $positional);
}


/*
tests('non-intrusive mode', function() {
	$greet = function($name, $greeting = 'Hello') {
		return "$greeting, $name!";
	};
	$greetF = fluentize($greet);
	expect('fluentize returns expected object',
		$greetF instanceof FluentArgs);
	expect('defaults work',
		$greetF->name('world'))
		->eq('Hello, world!');
	expect('state is saved, __invoke works',
		$greetF())
		->eq('Hello, world!');
	expect('can supply previously defaulted parameter',
		$greetF->greeting('Yo'))
		->eq('Yo, world!');
	expect('can overwrite previously applied parameter',
		$greetF->name('MTV Raps'))
		->eq('Yo, MTV Raps!');
	expect('resetting discards state',
		$greetF->reset()->name('Clarice')->call())
		->eq('Hello, Clarice!');
	expect('can supply both parameters',
		$greetF->greeting('Yo')->name('MTV Raps')->call())
		->eq('Yo, MTV Raps!');
	expect('can partially apply with an array',
		$greetF->call(['name' => 'Clarice']))
		->eq('Yo, Clarice!');
	expect('can fully apply with an array',
		$greetF->reset()->call(['name' => 'MTV Raps', 'greeting' => 'Yo']))
		->eq('Yo, MTV Raps!');
	expect('can apply with an array that is out of order w/r/t to the original position of the arguments',
		$greetF->reset()->call(['greeting' => 'Yo', 'name' => 'MTV Raps']))
		->eq('Yo, MTV Raps!');
});

class Intrusive
{
	public function test($args, $opts = NULL) {
		if (!is_array($args)) {
			return fluentize([$this, 'test'], $opts);
		}
		return $args;
	}
}


tests('intrusive mode', function() {
	// util to clean up invocations a tiny bit
	$inst = new Intrusive;
	$t = function($opts) use($inst) {
		return $inst->test(NULL, $opts);
	};
	// exception-checking callback that checks that there is a complaint about $col(s) of the nature indicated by $type
	$reqEx = function($col, $type) {
		$col = (array)$col;
		return function($str) use($col, $type) {
			return strpos($str, $type.' argument '.implode(', ', $col)) !== FALSE;
		};
	};

	expect('no options supplied, none required', function() use($t) {
		return $t([])->call();
	})->eq([]);
	expect('missing required arg complains', function() use($t) {
		return $t(['str' => ['required']])->call();
	})->throws('Exception', $reqEx('str', 'required'));
	expect('supplied required arg works', function() use($t) {
		return $t(['str' => ['required']])->str('some junk')->call();
	})->eq(['str' => 'some junk']);
	expect('multiple required', function() use($t) {
		return $t(['str' => ['required'], 'str2' => ['required']])->str2('some other junk')->str('some junk')->call();
	})->eq(['str' => 'some junk', 'str2' => 'some other junk']);
	expect('one required, one optional, both supplied', function() use($t) {
		return $t(['str' => ['required'], 'str2' => []])->str2('some other junk')->str('some junk')->call();
	})->eq(['str' => 'some junk', 'str2' => 'some other junk']);
	expect('one required, one optional, only required supplied', function() use($t) {
		return $t(['str' => ['required'], 'str2' => []])->str('some junk')->call();
	})->eq(['str' => 'some junk', 'str2' => NULL]);
	expect('one required, one optional, only optional supplied', function() use($t) {
		return $t(['str' => ['required'], 'str2' => []])->str2('some junk')->call();
	})->throws('Exception', $reqEx('str', 'required'));
});

 */
