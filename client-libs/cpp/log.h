/**
 * USAGE:
 *	// arbitrary name for the source of the message, automatically added to its
 *	// document
 * PgZmq::LogOpts opts("app name");
 * // create the logger instance from the configuration
 * auto log = opts
 * 	// URL usable by ZeroMQ to connect to the log server, default shown
 * 	.logPath("tcp://localhost:5555");
 *    // whether to install handler to close logs on sigint
 * 	.signalHandler(true)
 * 	// finish options
 * 	.logger();
 *
 * // then, log messages by calling the method corresponding to their severity:
 *	// log.access, log.emergency, log.error, log.debug, log.info, log.warning
 * // eg:
 * log.info()
 * 	// set object keys. some keys (application name, timestamp) are added
 * 	.set("something", "information")
 * 	.set("something else", "some other information")
 * 	// finish message
 * 	.send();
 * // ZeroMQ is asynchronous, to avoid losing messages log.close() should be
 * // called
 * log.close();
 */
#ifndef PG_ZMQ_LOGGER_CLIENT_H
#define PG_ZMQ_LOGGER_CLIENT_H

#include <algorithm>
#include <chrono>
#include <csignal>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>

#include <zmq.hpp>

#include "wrapidjson.h"

namespace PgZmq {

class Logger;

/**
 * option storage to fluentize API
 */
class LogOpts {
	friend class Logger;
	friend class LogMessage;
public:
	LogOpts(const std::string& name) : _appName(name), _logPath("tcp://localhost:5555"), _signalHandler(true) {
	}
	LogOpts() : LogOpts("") {}
	auto appName(const std::string& name) -> LogOpts& {
		_appName = name;
		return *this;
	}
	auto logPath(const std::string& path) -> LogOpts& {
		_logPath = path;
		return *this;
	}
	auto signalHandler(const bool on) -> LogOpts& {
		_signalHandler = on;
		return *this;
	}
	auto logger() -> Logger;
private:
	std::string _appName;
	std::string _logPath;
	bool _signalHandler;
};

class LogMessage;

/**
 * master control program
 */
class Logger {
	friend class LogOpts;
	friend class LogMessage;
	// doc recommends one thread per gb per second
	static const size_t IO_THREADS = 1;
public:
	/**
	 * send messages according to severity
	 */
	auto access()    -> LogMessage;
	auto debug()     -> LogMessage;
	auto info()      -> LogMessage;
	auto emergency() -> LogMessage;
	auto error()     -> LogMessage;
	auto warning()   -> LogMessage;

	/**
	 * clean up so process can exit
	 */
	auto close() -> void {
		if (logSock) {
			delete logSock;
		}
	}
private:
	Logger(const LogOpts& opts) : opts(opts), zctx(IO_THREADS), initialized(false) {
	}
	auto send(const std::string& doc) -> void {
		if (!initialized) {
			logSock = new zmq::socket_t(zctx, ZMQ_PUSH);
			logger = this;
			logSock->connect(opts._logPath.c_str());
			if (opts._signalHandler) {
				prevKilled = signal(SIGINT, Logger::killed);
			}
			initialized = true;
		}
		msg(doc);
	}
	void msg(const std::string& str) {
		zmq::message_t reply(str.size());
		memcpy(static_cast<void*>(reply.data()), str.data(), str.size());
		logSock->send(reply);
	}
	static void killed(int sigNum) {
		if (logger && sigNum == SIGINT) {
			logger->close();
		}
		if (prevKilled) {
			prevKilled(sigNum);
		}
	}
	static zmq::socket_t* logSock;
	static Logger* logger;
	static void (*prevKilled)(int);
	const LogOpts& opts;
	zmq::context_t zctx;
	bool initialized;
};

zmq::socket_t* Logger::logSock = nullptr;
Logger* Logger::logger = nullptr;
void (*Logger::prevKilled)(int) = nullptr;

/**
 * fluent builder for log messages
 */
class LogMessage {
public:
	LogMessage(Logger* logger, const std::string& level) : logger(logger), level(level), json(new Wrapid::ToJson), setKeys() {
	}
	LogMessage(const LogMessage&& other) : logger(other.logger), level(other.level), json(other.json), setKeys(other.setKeys) {
	}
	LogMessage(const LogMessage& other) = delete;
	auto operator=(const LogMessage&) -> LogMessage& = delete;
	~LogMessage() {
		delete json;
	}
	/**
	 * store a key/value pair
	 * where T is something the JSON helper class knows how to serialize
	 */
	template <typename T>
	auto set(const std::string& key, const T& val) -> LogMessage& {
		setKeys[key] = true;
		(*json)(key, val);
		return *this;
	}
	/**
	 * annotate the message with some automatic information and send it along
	 */
	auto send() -> void {
		set("app", logger->opts._appName);
		set("level", level);
		if (setKeys.find("timestamp") == setKeys.end()) {
			auto now = std::chrono::high_resolution_clock::now();
			auto nowC = std::chrono::system_clock::to_time_t(now);
			auto duration = now.time_since_epoch();

			std::stringstream ss;
			ss
				<< std::put_time(std::localtime(&nowC), "%Y-%m-%dT%H:%M:%S.")
				<< (std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() - (std::chrono::duration_cast<std::chrono::seconds>(duration).count() * 1000))
				<< std::put_time(std::localtime(&nowC), "%z")
				;
			set("timestamp", ss.str());
		}
		logger->send(json->toString());
	}
	/**
	 * alias for send()
	 */
	auto operator()() -> void {
		send();
	}
private:
	Logger* logger;
	const std::string level;
	Wrapid::ToJson* json;
	std::map<std::string, bool> setKeys;
};

auto Logger::access() -> LogMessage {
	return LogMessage(this, "access");
}
auto Logger::debug() -> LogMessage {
	return LogMessage(this, "debug");
}
auto Logger::info() -> LogMessage {
	return LogMessage(this, "info");
}
auto Logger::emergency() -> LogMessage {
	return LogMessage(this, "emergency");
}
auto Logger::error() -> LogMessage {
	return LogMessage(this, "error");
}
auto Logger::warning() -> LogMessage {
	return LogMessage(this, "warning");
}

auto LogOpts::logger() -> Logger {
	return Logger(*this);
}

}

#endif
