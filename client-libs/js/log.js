/**
 * USAGE:
 * let log = require('path/to/this/file/log.js')({
 * 	// arbitrary name for the source of the message, automatically added to
 * 	// its document
 * 	'app': 'foo',
 * 	// URL usable by ZeroMQ to connect to the log server, default shown
 * 	'log': 'tcp://localhost:5555',
 * 	// whether to hook for SIGINT to prevent message loss from killed
 * 	// processes
 * 	'signalHandler': true
 * });
 *
 * // the log object exposes methods named for the severity of the message:
 * // log.access, log.emergency, log.error, log.debug, log.info, log.warning
 *
 * // call these methods with an arbitrary serializable object:
 * log.info({'some': 'information', 'array': [1,2,3]});
 *
 * // it's important to synchronize before exiting the process to avoid losing
 * // messages. a handler for SIGINT is assigned to clean up for killed
 * // processes, and your program flow should otherwise result in a call to
 * // log.close(callback)
 * // you can do whatever you want in callback but if you don't call
 * // `process.exit` in it it won't.
 * log.close(process.exit);
 */
"use strict";
module.exports = (opts) => {
	opts.log  = opts.log  || 'ipc:///var/run/zmq-logger/log.sock';

	const
		zmq     = require('zmq'),
		logSock = zmq.socket('push'),
		log     = (type, payload) => {
			if (typeof payload == 'string') {
				payload = { 'message': payload };
			}
			// mild automatic annotation, not really required
			if (!payload.app) {
				payload.app = opts.app;
			}
			if (!payload.timestamp) {
				payload.timestamp = rv.now();
			}
			payload.level = type;
			logSock.send([type, JSON.stringify(payload)]);
		},
		rv = {
			// consistently-formatted timestamp (ISO 8601-extended)
			'now': () => {
				const
					dt  = new Date,
					tz  = -dt.getTimezoneOffset(),
					pad = (num) => {
						num = Math.abs(num);
						return num < 10 ? '0' + num : num;
					}
					;
				return dt.getFullYear() + '-' + pad(dt.getMonth() + 1) + '-' + pad(dt.getDate()) +
					'T' + pad(dt.getHours()) + ':' + pad(dt.getMinutes()) + ':' + pad(dt.getSeconds()) +
					 (tz >= 0 ? '+' : '-') + pad(tz / 60) + pad(tz % 60);
			},
			// sync with server to avoid losing messages.
			'close': (end) => {
				logSock.close();
			}
		}
		;
	logSock.connect(opts.log);

	['emergency', 'error', 'warning', 'debug', 'info', 'access'].forEach((type) => {
		rv[type] = (payload) => {
			log(type, payload);
		};
	});
	// zmq is multithreaded so we should explicitly try to get it to wrap things up
	// when it's time to do so, or it will lose messages
	if (opts.signalHandler === undefined || opts.signalHandler) {
		process.on('SIGINT', function() {
			rv.close();
		});
	}
	return rv;
};
